''' For upload files on upload. '''
# pylint: disable=R0903
import boto.s3.connection
from boto.s3.key import Key
#from yaml import load
#from stat import *
import os
#import time
import hashlib


class S3Uploader(object):
    '''    For upload on amazon.'''
    def __init__(self, aws_config, bucket, path):
        self.aws_config = aws_config
        conn = boto.s3.connection.S3Connection(aws_config['access_key'],
                                               aws_config['secret_key'])
        self.bucket = conn.get_bucket(bucket)
        self.path = path

    def upload_files(self):
        '''For upload on amazon.'''
        uploaded_files_count = 0

        src_dir = self.path
#        temp=(root, dirs, files)
        for temp in os.walk(src_dir):
            for file_name in temp[2]:
                filename = temp[0] + '/' + file_name
                rel_filepath = os.path.relpath(filename, src_dir)

                # Don't upload files from the hidden directories
                split_path = rel_filepath.split('/')
                temp = [file_path for file_path in split_path
                        if file_path.startswith('.')]
                if len(temp) > 0:
                    continue

                s3_file = self.bucket.get_key(rel_filepath)
                if s3_file is None:
                    s3_file = Key(self.bucket)
                    s3_file.key = rel_filepath

                # Upload file if there is a new version
                temp = hashlib.md5(open(filename, 'rb').read()).hexdigest()
                temp1 = "\"%s\"" % temp
                temp2 = s3_file.etag
                if s3_file.last_modified is None or temp1 != temp2:
                    fid = file(filename, 'r')
                    s3_file.set_contents_from_file(fid)
                    s3_file.set_acl('public-read')
                    uploaded_files_count += 1
                    print "  %s uploaded" % rel_filepath
#                else:
#                    print "  %s not changed" % rel_filepath

        return uploaded_files_count
